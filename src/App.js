import React from 'react';
import MainScreen from './screens/MainScreen';
import Country from './screens/Country';
import Page404 from './screens/Page404';
import { BrowserRouter as Router, Switch, Route} from 'react-router-dom';
function App() {
  return (
     <Router>
       <Switch>
         <Route exact path='/'>
           <MainScreen/>
         </Route>
         <Route path='/country/:name' children={<Country />} />
         <Route path='*' component={Page404} />
       </Switch>
     </Router>
  );
}

export default App;
