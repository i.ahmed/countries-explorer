import React, { useEffect } from 'react'
import {useGlobalContext} from '../context';
import SingleCountry from './SingleCountry';
import Loading from './Loading';
import NotFound from './NotFound';

const CountryList = () => {
    const {getCountries, countries, loading} = useGlobalContext();
    useEffect(() => {
        getCountries();
    }, [])
    return (
        <section className="container section">
            {loading 
            ? <Loading /> 
            : 
            <>
              {countries.length > 0 ? <div className="countries-list">
                {countries.map(country => {
                    if(country.name !== 'Israel'){
                        return <SingleCountry key={country.name} {...country}/> 
                    }
                    return null;
                })}
              </div>
              : <NotFound message='No Results Found'/>}
            </>
            }
        </section>
    );
}

export default CountryList
