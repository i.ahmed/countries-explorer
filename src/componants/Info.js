import React from 'react'
import {BsPeople, BsBuilding} from 'react-icons/bs';
import {HiOutlineLocationMarker, HiOutlineCurrencyDollar} from 'react-icons/hi';
import {GrLanguage} from 'react-icons/gr';
import {useGlobalContext} from '../context';
const Info = () => {
  const {countries} = useGlobalContext();
  const {name,population, capital, region,currencies, languages} = countries.length > 0 && countries[0];
    return (
        <table>
              <tbody>
                <tr>
                  <td>
                    <h3><BsPeople/> Name</h3>
                  </td>
                  <td><h3>{name || 'loading...'}</h3></td>
                </tr>
                <tr>
                  <td>
                    <h3><BsPeople/> Population</h3>
                  </td>
                  <td><h3>{population || 'loading...'}</h3></td>
                </tr>
                <tr>
                  <td><h3><BsBuilding/> Capital</h3></td>
                  <td><h3>{capital || 'loading...'}</h3></td>
                </tr>
                <tr>
                  <td><h3><HiOutlineLocationMarker/> Region</h3></td>
                  <td><h3>{region || 'loading...'}</h3></td>
                </tr>
                <tr>
                  <td><h3><HiOutlineCurrencyDollar/> Currencies</h3></td>
                  <td>
                    <ul>
                      {currencies && currencies.map((currence,index)=> {
                          const {name} = currence;
                          return <li key={index}>{name}</li>
                      })}
                    </ul>
                  </td>
                </tr>
                <tr>
                <td><h3><GrLanguage/> Languages</h3></td>
                <td>
                  <ul>
                    {languages && languages.map((language,index) => {
                        const {name} = language;
                        return <li key={index}>{name}</li>
                    })}
                  </ul>
                </td>
                </tr>
              </tbody>
            </table>
    )
}

export default Info
