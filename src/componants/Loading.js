import React from 'react'

const Loading = () => {
    return (
        <>
            <div className="loader"></div>
            <h4>Please Wait...</h4>
        </>
    )
}

export default Loading
