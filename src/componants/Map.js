import React, {useEffect, useState} from 'react'
import {MapContainer,MapConsumer, TileLayer, Marker, Popup} from 'react-leaflet';
import {useGlobalContext} from '../context';
const Map = () => {
    const {countries} = useGlobalContext();
    const [latlng, setLatlng] = useState([33,53]);
    const [name, setName] = useState('');
    
    useEffect(()=> {
        if(countries.length > 0 ){
            setLatlng(countries[0].latlng || latlng);
            setName(countries[0].name);
            // console.log(latlng);
        }
    }, [countries]);
    return (
        <MapContainer 
            center={latlng} 
            zoom={4}
            scrollWheelZoom={true}
            style={{height: '287px'}}>
            <MapConsumer>
                {(map)=> {
                    map.setView(latlng)
                    return (
                    <>
                    <TileLayer attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                    url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"/>
                    <Marker position={latlng}>
                        <Popup>
                            {name}
                        </Popup>
                    </Marker>
                    </>
                  )
                }}
            </MapConsumer>
        </MapContainer>
    )
}

export default Map
