import React from 'react';
import {useLocation, useHistory, Link} from 'react-router-dom';
import logo from '../images/logo.png';
import {BsSearch} from 'react-icons/bs';
import {useGlobalContext} from '../context';
const Navbar = () => {
    let {name, changeCountryName, getCountry} = useGlobalContext();
    const location = useLocation();
    const history = useHistory();
    const handleEvent = (e) => {
        if(e.key === 'Enter') {
            if(!name && !location.pathname.includes('country')){
                return getCountry(name);
            }else if(name && location.pathname.includes('country')) {
                getCountry(name);
               return history.push(`/country/${name}`);
            }  
            getCountry(name);
        }
    }
    return (
        <nav>
            <section className="container">
                <article className="brand">
                    <img src={logo} alt=""/>
                    <Link to="/">
                        <h2>Country Explorer</h2>
                    </Link>
                </article> 
                <article className="search">
                    <div className="wrapper">
                        <BsSearch className="search-icon"/>
                        <input 
                            type="text" 
                            placeholder="Search"
                            value={name} onChange={(e)=> 
                            changeCountryName(e.target.value)}
                            onKeyPress={handleEvent}/>
                    </div>
                </article>
            </section>
        </nav>
    )
}

export default Navbar
