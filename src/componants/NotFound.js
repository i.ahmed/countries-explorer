import React from 'react'
import {Link} from 'react-router-dom';
import NoData from '../images/no-data.png';

const NotFound = ({message}) => {
    return (
        <div className='not-found-container'>
            <img src={NoData} alt="no results" className="no-data"/>
            <h2>{message}</h2>
            <Link to='/'>
                <button>Go Back</button>
            </Link>
        </div>
    );
}

export default NotFound;
