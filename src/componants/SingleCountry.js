import React from 'react'
import {Link} from 'react-router-dom';
import {useGlobalContext} from '../context';
const SingleCountry = ({name, flag}) => {
    const { isSingleCountry } = useGlobalContext();
    return (
         <div className="single-country" style={{flex: `${isSingleCountry ? 'none' : '25%'}`}}>
             <img src={flag} alt={name} style={{width: `${isSingleCountry ? 'auto' : '100%'}`}}/>
             <div className="overlay">
                 <Link to={`/country/${name}`} className="link">
                     <h3>{name}</h3>
                 </Link>
             </div>
         </div>
    );
}

export default SingleCountry
