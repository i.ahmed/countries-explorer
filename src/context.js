import React , { useReducer, useContext } from 'react';
import reducer from './reducer';

const initState = {
    loading: false,
    name: '',
    isSingleCountry: false,
    countries: [],
    countryImages: []
};

const allCountriesURL  = 'https://restcountries.eu/rest/v2/all?fields=name;flag';
const countryByNameURL = 'https://restcountries.eu/rest/v2/name';

const AppContext = React.createContext(initState);
const AppProvider = ({children}) => {
    const [state, dispatch] = useReducer(reducer, initState);
    const getCountries = async () => {
        fetchData(allCountriesURL, 'COUNTRIES_FETCHED');
    }
    const getCountry = (name) => {
        if(name) {
            fetchData(`${countryByNameURL}/${name}`, 'SINGLE_COUNTRY_FETCHED');
        }else {
            getCountries();
        }
    }
    const fetchData = async (url, action) => {
        dispatch({type: 'LOADING'});
        const response = await fetch(`${url}`);
        
        const data =  response.state !== 404 ? await response.json() : [];
        dispatch({type: action, payload: data});
    }
    const changeCountryName = (name) => {
        dispatch({type: 'CHANGE_COUNTRY_NAME', payload: name});
    }
    return (
        <AppContext.Provider value={{
            getCountries,
            getCountry,
            changeCountryName,
            ...state
        }}>
            {children}
        </AppContext.Provider>
    );
}

const useGlobalContext = () => {
    return useContext(AppContext);
}

export { useGlobalContext , AppProvider };