const reducer = (state, action) => {
    if(action.type === 'LOADING'){
        return { ...state, loading: true};
    }else if(action.type === 'COUNTRIES_FETCHED') {
        return { ...state, loading: false, isSingleCountry: false ,countries: action.payload};
    }else if(action.type === 'CHANGE_COUNTRY_NAME') {
        return {...state, name: action.payload};
    }else if(action.type === 'SINGLE_COUNTRY_FETCHED') {
        return {...state, loading: false, isSingleCountry: true, countries: action.payload};
    }
}

export default reducer;