import React , {useEffect} from 'react'
import {useParams} from 'react-router-dom';
import Navbar from '../componants/Navbar';
import Map from '../componants/Map';
import Info from '../componants/Info';
import {useGlobalContext} from '../context';
import NotFound from '../componants/NotFound';

const Country = () => {
    const {getCountry, countries} = useGlobalContext();
    const {name} = useParams();
    useEffect(() => {
        getCountry(name.trim());
    },[name]);
    return (
        <>
          <Navbar/>
          {countries.length > 0 ?<section className="section-row">
            <Map/>
            <Info/>
          </section>
          :<NotFound message='No Results Found'/>}
        </>
    )
}

export default Country
