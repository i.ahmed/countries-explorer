import React from 'react'
import Navbar from '../componants/Navbar';
import CountryList from '../componants/CountryList';

const MainScreen = () => {
    return (
        <>
            <Navbar />
            <CountryList />
        </>
    )
}

export default MainScreen
