import React from 'react'
import NotFound from '../componants/NotFound';
const Page404 = () => {
    return (
        <>
            <NotFound message="404 Page Not Found"/>
        </>
    )
}

export default Page404

